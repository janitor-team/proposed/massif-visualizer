# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Simone Solinas <ksolsim@gmail.com>, 2011, 2012, 2013, 2014.
# Luigi Toscano <luigi.toscano@tiscali.it>, 2016.
#
msgid ""
msgstr ""
"Project-Id-Version: massif-visualizer\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2016-11-19 22:45+0100\n"
"PO-Revision-Date: 2016-10-15 22:30+0100\n"
"Last-Translator: Luigi Toscano <luigi.toscano@tiscali.it>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 2.0\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Simone Solinas"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "ksolsim@gmail.com"

#: app/callgraphtab.cpp:89
#, kde-format
msgid "Focus most expensive node"
msgstr "Focalizza il nodo più costoso"

#. i18n: ectx: ToolBar (callgraphToolBar)
#: app/callgraphtabui.rc:13
#, kde-format
msgid "CallGraph Toolbar"
msgstr "Barra degli strumenti CallGraph"

#: app/charttab.cpp:186
#, kde-format
msgid "Toggle total cost graph"
msgstr "Attiva/disattiva il grafico del costo totale"

#: app/charttab.cpp:192
#, kde-format
msgid "Toggle detailed cost graph"
msgstr "Attiva/disattiva il grafico del costo dettagliato"

#: app/charttab.cpp:200
#, kde-format
msgid "Stacked diagrams"
msgstr "Diagrammi a colonne"

#: app/charttab.cpp:203
#, kde-format
msgid "Stacked diagrams:"
msgstr "Diagrammi a colonne:"

#: app/charttab.cpp:212
#, kde-format
msgid "hide function"
msgstr "nascondi la funzione"

#: app/charttab.cpp:215
#, kde-format
msgid "hide other functions"
msgstr "nascondi altre funzioni"

#: app/charttab.cpp:280
#, kde-format
msgid "time in %1"
msgstr "tempo in %1"

#: app/charttab.cpp:287
#, kde-format
msgid "memory heap size"
msgstr "dimensione della memoria heap"

#: app/charttab.cpp:457
#, kde-format
msgid "Memory consumption of %1"
msgstr "Consumo di memoria di %1"

#: app/charttab.cpp:458
#, kde-format
msgid "Peak of %1 at snapshot #%2"
msgstr "Picco di %1 all'istantanea #%2"

#: app/charttab.cpp:460
#, kde-format
msgid ""
"Command: %1\n"
"Valgrind Options: %2"
msgstr ""
"Comando: %1\n"
"Opzioni Valgrind: %2"

#: app/charttab.cpp:465
#, kde-format
msgid "Save Current Visualization"
msgstr "Salva la visualizzazione corrente"

#: app/charttab.cpp:466
#, kde-format
msgid "Images (*.png *.jpg *.tiff *.svg)"
msgstr "Immagini (*.png *.jpg *.tiff *.svg)"

#: app/charttab.cpp:491
#, kde-format
msgid "Error, failed to save the image to %1."
msgstr "Errore, impossibile salvare l'immagine in %1."

#: app/charttab.cpp:503
#, kde-format
msgid "Massif Chart Print Preview"
msgstr "Anteprima di stampa del diagramma Massif"

#. i18n: ectx: ToolBar (chartToolBar)
#: app/charttabui.rc:17
#, kde-format
msgid "Chart Toolbar"
msgstr "Barra degli strumenti grafici"

#. i18n: ectx: property (text), widget (QLabel, costLabel)
#: app/config.ui:17
#, kde-format
msgid "Cost Precision:"
msgstr "Precisione del costo:"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox)
#: app/config.ui:27
#, kde-format
msgid "Legend"
msgstr "Legenda"

#. i18n: ectx: property (text), widget (QLabel, legendPositionLabel)
#: app/config.ui:36
#, kde-format
msgid "Position:"
msgstr "Posizione:"

#. i18n: ectx: property (text), item, widget (QComboBox, kcfg_LegendPosition)
#: app/config.ui:44
#, kde-format
msgid "North"
msgstr "Nord"

#. i18n: ectx: property (text), item, widget (QComboBox, kcfg_LegendPosition)
#: app/config.ui:49
#, kde-format
msgid "East"
msgstr "Est"

#. i18n: ectx: property (text), item, widget (QComboBox, kcfg_LegendPosition)
#: app/config.ui:54
#, kde-format
msgid "South"
msgstr "Sud"

#. i18n: ectx: property (text), item, widget (QComboBox, kcfg_LegendPosition)
#: app/config.ui:59
#, kde-format
msgid "West"
msgstr "Ovest"

#. i18n: ectx: property (text), item, widget (QComboBox, kcfg_LegendPosition)
#: app/config.ui:64
#, kde-format
msgid "Floating"
msgstr "Mobile"

#. i18n: ectx: property (text), widget (QLabel, legendAlignmentLabel)
#: app/config.ui:72
#, kde-format
msgid "Alignment:"
msgstr "Alineamento:"

#. i18n: ectx: property (text), item, widget (QComboBox, kcfg_LegendAlignment)
#: app/config.ui:80
#, kde-format
msgid "Left"
msgstr "Sinistra"

#. i18n: ectx: property (text), item, widget (QComboBox, kcfg_LegendAlignment)
#: app/config.ui:85
#, kde-format
msgid "Center"
msgstr "Centro"

#. i18n: ectx: property (text), item, widget (QComboBox, kcfg_LegendAlignment)
#: app/config.ui:90
#, kde-format
msgid "Right"
msgstr "Destra"

#. i18n: ectx: property (text), item, widget (QComboBox, kcfg_LegendAlignment)
#: app/config.ui:95
#, kde-format
msgid "Top"
msgstr "Alto"

#. i18n: ectx: property (text), item, widget (QComboBox, kcfg_LegendAlignment)
#: app/config.ui:100
#, kde-format
msgid "Bottom"
msgstr "Basso"

#. i18n: ectx: property (text), widget (QLabel, legendFontSizeLabel)
#: app/config.ui:108
#, kde-format
msgid "Font Size:"
msgstr "Dimensione carattere:"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_ShortenTemplates)
#. i18n: ectx: label, entry (ShortenTemplates), group (Settings)
#: app/config.ui:121 app/mainwindow.cpp:189
#: app/massif-visualizer-settings.kcfg:14
#, kde-format
msgid "Shorten Templates"
msgstr "Abbrevia i modelli"

#: app/configdialog.cpp:41
#, kde-format
msgid "Settings"
msgstr "Impostazioni"

#: app/documentwidget.cpp:113
#, kde-format
msgid "loading file <i>%1</i>..."
msgstr "caricamento file <i>%1</i>..."

#: app/documentwidget.cpp:193
#, kde-format
msgid "Memory Chart"
msgstr "Grafico della memoria"

#: app/documentwidget.cpp:201
#, kde-format
msgid "Callgraph"
msgstr "Callgraph"

#: app/documentwidget.cpp:207
#, kde-format
msgid "Allocators"
msgstr "Allocatori"

#: app/main.cpp:40
#, kde-format
msgid "Massif Visualizer"
msgstr "Visualizzatore Massif"

#: app/main.cpp:41
#, kde-format
msgid "A visualizer for output generated by Valgrind's massif tool."
msgstr ""
"Un visualizzatore per l'output generato dallo strumento massif di Valgrind."

#: app/main.cpp:42
#, kde-format
msgid "Copyright 2010-2015, Milian Wolff <mail@milianw.de>"
msgstr "Copyright 2010-2015, Milian Wolff <mail@milianw.de>"

#: app/main.cpp:44
#, kde-format
msgid "Milian Wolff"
msgstr "Milian Wolff"

#: app/main.cpp:44
#, kde-format
msgid "Original author, maintainer"
msgstr "Autore originale, responsabile"

#: app/main.cpp:47
#, kde-format
msgid "Arnold Dumas"
msgstr "Arnold Dumas"

#: app/main.cpp:47
#, kde-format
msgid "Multiple document interface, bug fixes"
msgstr "Interfaccia a documenti multipli, correzione bug"

#: app/main.cpp:65
#, kde-format
msgid "Files to load"
msgstr "File da caricare"

#: app/main.cpp:65
#, kde-format
msgid "[FILE...]"
msgstr "[FILE...]"

#: app/mainwindow.cpp:195
#, kde-format
msgid "Select peak snapshot"
msgstr "Istantanea del picco selezionato"

#: app/mainwindow.cpp:201
#, kde-format
msgid "add"
msgstr "aggiungi"

#: app/mainwindow.cpp:202
#, kde-format
msgid "add custom allocator"
msgstr "aggiungi un allocatore personalizzato"

#: app/mainwindow.cpp:205
#, kde-format
msgid "remove"
msgstr "rimuovi"

#: app/mainwindow.cpp:207
#, kde-format
msgid "remove selected allocator"
msgstr "rimuovi l'allocatore personalizzato"

#: app/mainwindow.cpp:214
#, kde-format
msgid "mark as custom allocator"
msgstr "segna come allocatore personalizzato"

#: app/mainwindow.cpp:225
#, kde-format
msgid "Open Massif Data File"
msgstr "Apri il file dati di Massif"

#: app/mainwindow.cpp:257
#, kde-format
msgid "Open Massif Output File"
msgstr "Apri il file di output di Massif"

#: app/mainwindow.cpp:258
#, kde-format
msgid "Massif data files (massif.out.*)"
msgstr "File di dati di Massif (massif.out.*)"

#: app/mainwindow.cpp:395
#, kde-format
msgid "Add Custom Allocator"
msgstr "Aggiungi un allocatore personalizzato"

#: app/mainwindow.cpp:395
#, kde-format
msgid "allocator:"
msgstr "allocatore:"

#: app/mainwindow.cpp:479
#, kde-format
msgid "Evaluation of %1 (%2)"
msgstr "Valutazione di %1 (%2)"

#. i18n: ectx: property (text), widget (QToolButton, openFile)
#: app/mainwindow.ui:67
#, kde-format
msgid "..."
msgstr "..."

#. i18n: ectx: property (windowTitle), widget (QDockWidget, dataTreeDock)
#: app/mainwindow.ui:109
#, kde-format
msgid "Massif Data"
msgstr "Dati Massif"

#. i18n: ectx: property (clickMessage), widget (KLineEdit, filterDataTree)
#: app/mainwindow.ui:122
#, kde-format
msgid "filter"
msgstr "filtro"

#. i18n: ectx: property (windowTitle), widget (QDockWidget, allocatorDock)
#: app/mainwindow.ui:137
#, kde-format
msgid "Custom Allocators"
msgstr "Allocatore personalizzato"

#. i18n: ectx: label, entry (PrettyCostPrecision), group (Settings)
#: app/massif-visualizer-settings.kcfg:9
#, kde-format
msgid "Cost Precision"
msgstr "Precisione del costo"

#. i18n: ectx: tooltip, entry (PrettyCostPrecision), group (Settings)
#: app/massif-visualizer-settings.kcfg:10
#, kde-format
msgid ""
"Defines the number of places after the comma for memory costs shown in the "
"application."
msgstr ""
"Definisce il numero di posti dopo la virgola per i costi di memoria mostrati "
"nell'applicazione."

#. i18n: ectx: tooltip, entry (ShortenTemplates), group (Settings)
#: app/massif-visualizer-settings.kcfg:15
#, kde-format
msgid ""
"Defines whether identifiers of C++ template instantiations should be "
"shortened by removing their template arguments."
msgstr ""
"Definisce se gli identificatori delle istanze dei template in C++ devono "
"essere accorciati rimuovendo i loro argomenti."

#. i18n: ectx: label, entry (LegendPosition), group (Settings)
#: app/massif-visualizer-settings.kcfg:19
#, kde-format
msgid "Legend Position"
msgstr "Posizione della legenda:"

#. i18n: ectx: tooltip, entry (LegendPosition), group (Settings)
#: app/massif-visualizer-settings.kcfg:20
#, kde-format
msgid "Defines the position of the legend relative to the plot."
msgstr "Definisce la posizione della legenda relativa al disegno."

#. i18n: ectx: label, entry (LegendAlignment), group (Settings)
#: app/massif-visualizer-settings.kcfg:31
#, kde-format
msgid "Legend Alignment"
msgstr "Alineamento della legenda"

#. i18n: ectx: tooltip, entry (LegendAlignment), group (Settings)
#: app/massif-visualizer-settings.kcfg:32
#, kde-format
msgid "Defines the alignment of the legend."
msgstr "Definisce l'allineamento della legenda."

#. i18n: ectx: label, entry (LegendFontSize), group (Settings)
#: app/massif-visualizer-settings.kcfg:45
#, kde-format
msgid "Legend Font Size"
msgstr "Dimensione del carattere della legenda"

#. i18n: ectx: tooltip, entry (LegendFontSize), group (Settings)
#: app/massif-visualizer-settings.kcfg:46
#, kde-format
msgid "Defines the font size used in the legend."
msgstr "Definisce la dimensione del carattere della legenda."

#. i18n: ectx: Menu (file)
#: app/massif-visualizerui.rc:5
#, kde-format
msgid "&File"
msgstr "&File"

#. i18n: ectx: Menu (view)
#: app/massif-visualizerui.rc:19
#, kde-format
msgid "&View"
msgstr "&Vista"

#. i18n: ectx: Menu (settings)
#: app/massif-visualizerui.rc:23
#, kde-format
msgid "&Settings"
msgstr "Impo&stazioni"

#. i18n: ectx: Menu (dockWidgets)
#: app/massif-visualizerui.rc:25
#, kde-format
msgid "&Dock Widgets"
msgstr "Oggetti &da agganciare"

#. i18n: ectx: Menu (help)
#: app/massif-visualizerui.rc:34
#, kde-format
msgid "&Help"
msgstr "&Aiuto"

#. i18n: ectx: ToolBar (mainToolBar)
#: app/massif-visualizerui.rc:45
#, kde-format
msgid "Main Toolbar"
msgstr "Barra degli strumenti principale"

#: massifdata/parseworker.cpp:55
#, kde-format
msgid "Download Failed"
msgstr "Download non riuscito"

#: massifdata/parseworker.cpp:56
#, kde-format
msgid "Failed to download remote massif data file <i>%1</i>."
msgstr "Impossibile scaricare il file di dati remoto di massif <i>%1</i>."

#: massifdata/parseworker.cpp:66
#, kde-format
msgid "Read Failed"
msgstr "Lettura non riuscita"

#: massifdata/parseworker.cpp:67
#, kde-format
msgid "Could not open file <i>%1</i> for reading."
msgstr "Impossibile aprire il file <i>%1</i> in lettura."

#: massifdata/parseworker.cpp:78
#, kde-format
msgid "Parser Failed"
msgstr "Analisi non riuscita"

#: massifdata/parseworker.cpp:79
#, kde-format
msgid "Could not parse file <i>%1</i>.<br>Parse error in line %2:<br>%3"
msgstr ""
"Impossibile analizzare il file <i>%1</i>.<br>Errore nell'analisi alla riga "
"%2:<br>%3"

#: massifdata/parseworker.cpp:86
#, kde-format
msgid "Empty data file <i>%1</i>."
msgstr "File di dati vuoto <i>%1</i>."

#: massifdata/parseworker.cpp:87
#, kde-format
msgid "Empty Data File"
msgstr "File di dati vuoto"

#: massifdata/util.cpp:136
#, kde-format
msgid "<dt>function:</dt><dd>%1</dd>\n"
msgstr "<dt>funzione:</dt><dd>%1</dd>\n"

#: massifdata/util.cpp:139
#, kde-format
msgid "<dt>location:</dt><dd>%1</dd>\n"
msgstr "<dt>posizione:</dt><dd>%1</dd>\n"

#: massifdata/util.cpp:142
#, kde-format
msgid "<dt>address:</dt><dd>%1</dd>\n"
msgstr "<dt>indirizzo:</dt><dd>%1</dd>\n"

#: massifdata/util.cpp:155
#, kde-format
msgid "<dt>cost:</dt><dd>%1, i.e. %2% of snapshot #%3</dd>"
msgstr "<dt>costo:</dt><dd>%1, cioè %2% di istantanea #%3</dd>"

#: visualizer/allocatorsmodel.cpp:127
#, kde-format
msgid "<dt>peak cost:</dt><dd>%1</dd>"
msgstr "<dt>costo del picco:</dt><dd>%1</dd>"

#: visualizer/allocatorsmodel.cpp:137
#, kde-format
msgid "below threshold"
msgstr "sotto la soglia"

#: visualizer/allocatorsmodel.cpp:160
#, kde-format
msgid "Function"
msgstr "Funzione"

#: visualizer/allocatorsmodel.cpp:162
#, kde-format
msgid "Location"
msgstr "Percorso"

#: visualizer/allocatorsmodel.cpp:164
#, kde-format
msgid "Peak"
msgstr "Picco"

#: visualizer/datatreemodel.cpp:84
#, kde-format
msgid "Snapshots"
msgstr "Istantanee"

#: visualizer/datatreemodel.cpp:148
#, kde-format
msgid "Peak snapshot: heap cost of %1"
msgstr "Istantanea del picco: costo del heap di %1"

#: visualizer/datatreemodel.cpp:150
#, kde-format
msgid "Snapshot #%1: heap cost of %2"
msgstr "Istantanea #%1: costo heap di %2"

#: visualizer/datatreemodel.cpp:153
#, kde-format
msgctxt "%1: snapshot number"
msgid "Snapshot #%1"
msgstr "Istantanea #%1"

#: visualizer/datatreemodel.cpp:160
#, kde-format
msgctxt "%1: cost, %2: snapshot number"
msgid "%1: Snapshot #%2 (peak)"
msgstr "%1: Istantanea #%2 (picco)"

#: visualizer/datatreemodel.cpp:163
#, kde-format
msgctxt "%1: cost, %2: snapshot number"
msgid "%1: Snapshot #%2"
msgstr "%1: Istantanea #%2"

#: visualizer/datatreemodel.cpp:175
#, kde-format
msgctxt "%1: cost, %2: snapshot label (i.e. func name etc.)"
msgid "%1: %2"
msgstr "%1: %2"

#: visualizer/dotgraphgenerator.cpp:198
#, kde-format
msgid "snapshot #%1 (taken at %2%4)\\nheap cost: %3"
msgstr "Istantanea #%1 (presa alle %2%4)\\ncosto heap: %3"

#: visualizer/dotgraphgenerator.cpp:303
#, kde-format
msgid "in one place below threshold"
msgid_plural "in %1 places, all below threshold"
msgstr[0] "in un posto al di sotto della soglia"
msgstr[1] "in %1 posti, tutti al di sotto della soglia"

#: visualizer/totalcostmodel.cpp:79
#, kde-format
msgid "Total Memory Heap Consumption"
msgstr "Consumo totale della memoria heap"

#: visualizer/totalcostmodel.cpp:123
#, kde-format
msgid ""
"Snapshot #%1:\n"
"Heap cost of %2\n"
"Extra heap cost of %3\n"
"Stack cost of %4"
msgstr ""
"Istantanea #%1:\n"
"Costo heap di %2\n"
"Costo extra heap di %3\n"
"Costo dello stack di %4"